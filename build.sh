#!/bin/bash

set -eo pipefail

VERSION=${VERSION:-"1.4.0"}
RELEASE=${RELEASE:-"1"}
URL="https://releases.hashicorp.com/consul/${VERSION}/consul_${VERSION}_linux_amd64.zip"

build() {
  pushd rootfs/
    fpm -n consul \
    -m "williecadete@gmail.com" \
    --rpm-summary "Consul is a distributed, highly available, and data center aware solution to connect and configure applications across dynamic, distributed infrastructure." \
    --description "Consul is a tool for service discovery and configuration. Consul is distributed, highly available, and extremely scalable.

    Consul provides several key features:

    Service Discovery - Consul makes it simple for services to register themselves and to discover other services via a DNS or HTTP interface. External services such as SaaS providers can be registered as well.

    Health Checking - Health Checking enables Consul to quickly alert operators about any issues in a cluster. The integration with service discovery prevents routing traffic to unhealthy hosts and enables service level circuit breakers.

    Key/Value Storage - A flexible key/value store enables storing dynamic configuration, feature flagging, coordination, leader election and more. The simple HTTP API makes it easy to use anywhere.

    Multi-Datacenter - Consul is built to be datacenter aware, and can support any number of regions without complex configuration.

    Service Segmentation - Consul Connect enables secure service-to-service communication with automatic TLS encryption and identity-based authorization.
" \
    --url "https://www.consul.io/" \
    --license "Mozilla Public License 2.0" \
    -s dir \
    -t rpm \
    --iteration $RELEASE\
    -v $VERSION \
    -p ../ \
    --prefix / \
    etc usr
  popd
}

get_tarball(){
  test -d ./rootfs/usr/sbin || mkdir ./rootfs/usr/sbin
  test -f /tmp/consul*.zip || wget -q $URL -P /tmp/
  unzip /tmp/consul_${VERSION}_linux_amd64.zip -d ./rootfs/usr/sbin
}

get_tarball && build